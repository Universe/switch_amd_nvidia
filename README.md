# `switch-amd-nvidia` - Scripts to switch between AMD and NVIDIA GPU for Artix Linux

![Screenshot](https://gitea.artixlinux.org/Universe/switch_amd_nvidia/raw/branch/master/Nvidia-logo.png)

To switch :

* <code>sudo switch_vers_amd</code>, to switch to AMD GPU
* <code>sudo switch_vers_nvidia</code>, to switch to AMD GPU
 
> Needs a clean configuration and no /etc/X11/xorg.conf file.
>
> nouveau must be blacklisted for this to work  

